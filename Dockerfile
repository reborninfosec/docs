FROM nginx
ADD  web/site /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]