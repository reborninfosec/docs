---
title: 'Welcome to RebornInfosec'
---

```
    ____       __                        _       ____                    
   / __ \___  / /_  ____  _________     (_)___  / __/___  ________  _____
  / /_/ / _ \/ __ \/ __ \/ ___/ __ \   / / __ \/ /_/ __ \/ ___/ _ \/ ___/
 / _, _/  __/ /_/ / /_/ / /  / / / /  / / / / / __/ /_/ (__  )  __/ /__  
/_/ |_|\___/_.___/\____/_/  /_/ /_/  /_/_/ /_/_/  \____/____/\___/\___/  
```                                                                   
<!-- [![Website shields.io](https://img.shields.io/website-up-down-green-red/http/shields.io.svg)](http://shields.io/) [![Generic badge](https://img.shields.io/badge/Ask%20me-Infosec-<COLOR>.svg)](https://shields.io/) [![made-with-Markdown](https://img.shields.io/badge/Made%20with-Markdown-1f425f.svg)](http://commonmark.org) [![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/) -->


Hi :wave:, This is **Varun Kondagadapa** and welcome to my blog reborninfosec. I have planned this blog to express my view on Information security and will try to `learn from`/`help` other security minds.              

`head about.me`:point_down:

Information Security Specialist with 5+ years of experience in Securing organizations. Specialty in automating Cyber security life-cycle in various areas like software development, computer networks and human factor. Extensively worked in DevSecOps domain and built tools for the community.

`---more---`

<!-- - [![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://github.com/ellerbrock/open-source-badges/) -->

- Dignity of labor
- Ethics
- Equality
- Minimalism

:e-mail:`Colab`:satellite::bulb::

- LinkedIn: <a href="https://www.linkedin.com/in/varunkondagadapa" target="_blank">Varun Kondagadapa</a>
- GitLab: <a href="https://gitlab.com/reborninfosec" target="_blank">reborninfosec</a>
- Twitter: <a href="https://twitter.com/reborninfosec" target="_blank">reborninfosec</a>
- Telegram: <a href="https://t.me/reborninfosec" target="_blank">reborninfosec</a>

<!-- `lsb_release -a`
!["Architecture"](./images/auto_reborn_site.png) -->

!!! Legal
    All views expressed here are strictly from personal view. This blog is neither an organization nor affiliated to any organization with respect to the blog posts.
