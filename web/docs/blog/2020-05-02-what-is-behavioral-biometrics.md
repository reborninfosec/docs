---
layout: post
title: 'What is Behavioral biometrics?'
---
<h2 id="what-is-it-">What is it?</h2>
<p>Behavioral bio-metrics is a model which determine &quot;us&quot; based on the pattern we follow while doing mouse clicks, keyboard type, time taken to swipe etc. It is derived from our physical bio-metrics where our finger print is the pattern. Here in behavioral bio-metrics our activity is the pattern.</p>
<h2 id="applications-">Applications.</h2>
<p>This can be used in security in many ways:</p>
<ul>
<li>We can create authentication system</li>
<li>Find the anomalies</li>
<li>Prevent the advanced USB attacks</li>
<li>MouseJack attacks etc.</li>
</ul>
<h2 id="implementation-">Implementation:</h2>
<p>There were very few companies working on implementation. One can give a try for implementation but this would be the toughest thing to work on because model should be utmost accurate to determine the personality.</p>
