---
layout: post
title: 'How a feature in an application can be a curse'
---
<h3 id="scenario-">Scenario:</h3>
<p>2018 December; researchers found a vulnerability in facebook which effects almost 50 million accounts. The main root cause of issue is a new feature in facebook app which lets people see what their own profile looks like to someone else. Facebook initiated incident response for checking whether accounts were breached or not.</p>
<h3 id="beware-of-features-">Beware of features:</h3>
<p>Say we have developed a product which satisfies a simple need. If the same product got the functionality to deliver extra benefits then it is called a feature. Now features are always good for increasing the market value of that product, but those are the things we give less security preference. Why? Because a product before rolling out will be rigorously tested and the core functionality of that product was built for one kind of purpose. These features are added on top of product but not with the product core purpose and tested for only integration purpose.</p>
<p>Things to consider while adding features:</p>
<ul>
<li>Continuous unit testing</li>
<li>Continuous security testing</li>
<li>Continuous functional testing</li>
<li>Implementing Attacker testing strategies</li>
</ul>
