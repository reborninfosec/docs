---
layout: post
title: 'Whom to blame if I get hacked'
---
<h3 id="story">Story</h3>
<p>Security Alert: Your Google account was just signed in to from a new &quot;Linux&quot; device. 
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1559384929398/e00qjIRNl.jpeg" alt="Security_alert.jpg">
Yeah, I got the above alert as mail yesterday but I have seen that today. What does it mean? It is saying that someone has logged into my google account and it might be me or some one who knows my password. The first thing I did when I saw this mail is expanded the &quot;view details&quot; option on the top. Why? Am I a mad person? No, what if this is a phishing mail and crafted similarly as google page? Then you will be landing into a phishing page and give your credentials assuming it is a legitimate Gmail page. I don&#39;t want to get trapped. Finally I did verified that the email is from Google source, opened &quot;My Activity&quot; and came to know that some one logged in from XYZ country. Yeah my Gmail account got hacked.</p>
<h4 id="5-stages-of-grief">5 Stages of Grief</h4>
<p> OK, OK it&#39;s done and I turned to follow  all the stages of grief...</p>
<ol>
<li><strong>Denial</strong> - No it&#39;s not happened</li>
<li><p><strong>Anger</strong> - It&#39;s already been two days since my account got hacked and I don&#39;t know. Which means hacker might have accessed all my bank accounts, my photos, locations and all other website accounts linked with that email.
Now I&#39;m looking for the parties in anger to know by whom this hack has happened.</p>
<ul>
<li>The guys who sent me the &quot;Bank loan, job offers, crypto trading, porn.. etc&quot; offers every day</li>
<li>The location from where I found a &quot;Apple&quot; usb drive</li>
<li>The person who sat next to me and also did shoulder surfing</li>
<li>The man called be asking for OTP</li>
<li>Email provider - Who failed to secure my account.</li>
</ul>
</li>
<li><p><strong>Bargaining</strong> - I tried to find all the first 4 parties, I haven&#39;t found them still.Then I started inquiring email vendor to sue them. That makes me happy at this moment. But no luck, I got a legal negative response, which is not expected.</p>
</li>
<li><p><strong>Depression</strong> - Yeah I&#39;m feeling bad,low,depressed because its my personality which is exposed to public. Hacker even know my browsing history by which one can easily define what kind of a person Iam. I&#39;m not private now, I have nothing to hide, what if hacker disclose everything to public? All the things which I only know will be known by everyone. Anyone can do Cyber-extortion.</p>
</li>
<li><p><strong>Acceptance</strong> - Yeah I know I got hacked, I have to come out of this. For this I started figuring out where exactly the issue is. After looking at the scenarios like Phishing mails I clicked, USB&#39;s inserted into my PC, entering passwords in Internet cafe, Telling OTP&#39;s over phone to stranger, Not maintaining strong passwords, Typing sensitive data while traveling in public train.... Any single/simple reason might turned-out to this serious issue. </p>
</li>
</ol>
<h3 id="who-">WHO?</h3>
<p>And yeah finally, It&#39;s me who failed in any one of above scenarios. May be I could have tried enabling 2FA, different/strong passwords and being alert when I get offer mails. I forgot the basic principle in life that &quot;Nothing comes for Free, not even an email&quot;. </p>
<blockquote>
<p>Yes, it&#39;s me who should be blamed for being the reason of getting my account hacked.</p>
</blockquote>
