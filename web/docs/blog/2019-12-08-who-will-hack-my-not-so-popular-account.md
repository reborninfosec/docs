---
layout: post
title: 'who will hack my not so popular account'
---
<p>&#39;Who will hack me&#39;, &#39;I&#39;m not in that radar&#39; , &#39;nothing will be lost even if I got hacked&#39;..etc.. we keep on listening these dialogues very often. But the bitter truth is people can be hacked without any reason. OK let&#39;s see the attack types.</p>
<h3 id="targeted-attack-">Targeted Attack:</h3>
<p>This kind of attack is done by proper foot-printing of a target. Anyone can be a target if he/she has some wealth, competitor, crime story, secrets etc. There will be very less chance to escape from these attacks because targeted attacks have every information about the target. It may start from performing spear phishing and eventually become an advanced persistent threat.</p>
<h3 id="mass-breach-attack-">Mass breach/attack:</h3>
<p>If a public website which holds thousands of accounts has a vulnerability, then our accounts in that website are also hacked. If we are not sure whether we got hacked or not, checkout the below link.
https://haveibeenpwned.com/</p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1575782002005/2Ilz_IN1G.png" alt="hibp.png"></p>
<p>This website collects all the emails and passwords from security breaches of various web applications. If we submit our mail-id/password then it will check the database whether our details are available in the breach or not. If we find our accounts in the above database, we must got hacked sometime. May be the attacker not targeted you but finally you got hacked. It&#39;s our perception that we  will never get hacked, there is no necessity of being well known but a simple identity in the Internet is enough.</p>
