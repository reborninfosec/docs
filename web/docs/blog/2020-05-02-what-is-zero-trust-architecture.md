---
layout: post
title: 'what is Zero trust architecture?'
---
<h2 id="trust-no-one-">Trust No one:</h2>
<p>Traditionally a perimeter based Environment is something like below.
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588442907196/aWE64xAIA.png" alt="Perimeter-Network.png">
There is intranet, all the corporate users are part of one single network and security is provided by perimeter layer security systems such as firewalls etc.. Here in this model we are trusting the internal users of corporate LAN and access to servers are given based on VLAN&#39;s, NACLs etc. Most of the time if any intranet system got compromised then there would be 99% risk for the other assets in the same network. This is the legacy model where people are following, but now and in future there would be no perimeter. Employees are roaming, Corporate access from mobile devices are the few challenges to solve with security. We cannot take control of Internet and based on that idea Zero trust model has started</p>
<p>Look at the following image:
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588443045401/WIRN-rAym.png" alt="Zero_trust.png">
Every corporate device, either it can be a mobile,server,desktop or laptop will access the resources through the INTERNET. There is no perimeter and no network level acl&#39;s etc. In this model all the access control is based on Identity and Access Management suites. No one trust the other person, based on the IAM roles, Users will access the resources. This is just like the AAA(Authentication, Authorization, Account) framework but implemented from more ground level protocols.</p>
