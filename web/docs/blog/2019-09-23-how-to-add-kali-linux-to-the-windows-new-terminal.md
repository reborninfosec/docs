---
layout: post
title: 'How to add Kali Linux to the windows new terminal'
---
<p>Windows new terminal is rocking and switching between the powershell/bash is made easy than ever. Installation of new windows terminal can be done by following their GitHub link:  <a target='_blank' rel='noopener noreferrer'  href="Link">https://github.com/microsoft/terminal</a> . Adding the Kali linux to the wsl is done using windows store. Follow the document to add kali to windows subsystem library:  <a target='_blank' rel='noopener noreferrer'  href="Link">https://www.kali.org/news/kali-linux-in-the-windows-app-store/</a>.</p>
<h2 id="scenario-">Scenario:</h2>
<p>As a penetration tester we always wanted kali linux in windows machine. Always installing kali in the virtualbox is not the optimal solution. If kali is also available in windows store then why anyone should waste time in installing kali in virtual machine. By using this new windows terminal one can add kali to the terminal and can switch between windows powershell and kali terminal like ever before. </p>
<ul>
<li><p>Step1:
Install Windows terminal and Kali wsl using the above links.</p>
</li>
<li><p>Step2: 
Before changing the settings of terminal we need uuid of kali linux instance.
Enter the following commands in powershell to get values from registry.</p>
</li>
</ul>
<pre><code>(Get-ChildItem -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\AppModel\StateRepository\<span class="hljs-keyword">Cache</span>\PackageFamily\<span class="hljs-keyword">Data</span> | findstr.exe <span class="hljs-string">"Kali"</span>).<span class="hljs-keyword">Substring</span>(<span class="hljs-number">0</span>,<span class="hljs-number">2</span>).<span class="hljs-keyword">Trim</span>()
</code></pre><p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1569253492914/3gNtsz5Ks.png" alt="powershell2.PNG">
Take the output of the command and put it the next command. In my case the output is &quot;<strong>6f</strong>&quot;
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1569253993110/fOUvOAHq1.png" alt="powershell3.png"></p>
<pre><code>Get-ChildItem -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\AppModel\StateRepository\<span class="hljs-keyword">Cache</span>\PackageFamily\<span class="hljs-keyword">Data</span> | <span class="hljs-keyword">where</span> {$_.<span class="hljs-keyword">Name</span> -<span class="hljs-keyword">match</span> <span class="hljs-string">"6f"</span> }
</code></pre><ul>
<li>Step3:
Note down the Publisher value(Without &quot;<strong>CN=</strong>&quot;) from the output of the above command. This is the value we insert for our <strong>guid</strong>.</li>
</ul>
<ul>
<li>Step4:
Open terminal settings by accessing options in the right pane.
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1569253230750/TDlQBDsXZ.png" alt="powershell1.png"></li>
</ul>
<ul>
<li>Step5: 
Settings file is opened in your favorite editor and insert the values like below after ubuntu record</li>
</ul>
<pre><code>        {
            "<span class="hljs-attr">acrylicOpacity</span>" : <span class="hljs-number">0.5</span>,
            "<span class="hljs-attr">closeOnExit</span>" : <span class="hljs-literal">true</span>,
            "<span class="hljs-attr">colorScheme</span>" : <span class="hljs-string">"Campbell"</span>,
            "<span class="hljs-attr">commandline</span>" : <span class="hljs-string">"wsl.exe -d kali-linux"</span>,
            "<span class="hljs-attr">cursorColor</span>" : <span class="hljs-string">"#FFFFFF"</span>,
            "<span class="hljs-attr">cursorShape</span>" : <span class="hljs-string">"bar"</span>,
            "<span class="hljs-attr">fontFace</span>" : <span class="hljs-string">"Consolas"</span>,
            "<span class="hljs-attr">fontSize</span>" : <span class="hljs-number">10</span>,
            "<span class="hljs-attr">guid</span>" : <span class="hljs-string">"{**guid value**}"</span>,
            "<span class="hljs-attr">historySize</span>" : <span class="hljs-number">9001</span>,
            "<span class="hljs-attr">icon</span>" : <span class="hljs-string">"**Your Png logo**"</span>,
            "<span class="hljs-attr">name</span>" : <span class="hljs-string">"Kali-Linux"</span>,
            "<span class="hljs-attr">padding</span>" : <span class="hljs-string">"0, 0, 0, 0"</span>,
            "<span class="hljs-attr">snapOnInput</span>" : <span class="hljs-literal">true</span>,
            "<span class="hljs-attr">useAcrylic</span>" : <span class="hljs-literal">false</span>
        }
</code></pre><p>My settings file for the reference:  <a target='_blank' rel='noopener noreferrer'  href="Link">https://pastebin.com/C08wW1xs</a> </p>
<ul>
<li>Step6: 
Finally access the kali terminal side by side with windows powershell.</li>
</ul>
<p>This could help security professionals who are using windows operating system to switch kali with a single tab key. </p>
