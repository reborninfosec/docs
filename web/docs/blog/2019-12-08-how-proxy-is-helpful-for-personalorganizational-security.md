---
layout: post
title: 'How proxy is helpful for personal/organizational security'
---
<h3 id="an-intermediate-">An intermediate:</h3>
<p>The original meaning of proxy is &quot;an intermediate&quot;. In computer networks we can use proxies to hide our identity. This can be a server identity or a client identity.</p>
<h3 id="serverside-">Serverside:</h3>
<p>We use reverse proxy from server side. This also acts as a request handler. Whatever request server receives, this proxy will take that request and handle to actual server. OK then how it is gonna help? we might have heard about shodan which is a search engine for Internet connected devices, the moment we make our server up then these search engine bots automatically does the basic reconnaissance. That includes banner grabbing, server version, technologies used etc. To escape from these type of attacks we can choose a reverse proxy which keep our servers from automated attacks.</p>
<h3 id="clientside-">Clientside:</h3>
<p>Yeah, we also need forward proxy from client side. Forward proxy can control our outbound traffic. Why do we need this? Well, sometimes we end up with phishing attacks by clicking malicious links. To secure ourselves from these attacks, we can use forward proxy and create some api&#39;s from proxy level. So when you click any link, the request goes to forward proxy and proxy verifies whether the destination is legitimate or malicious using api&#39;s like virustotal. If it is legit then proxy passes the request and it will block the rest.</p>
