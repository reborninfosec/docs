---
layout: post
title: 'Whatsapp in internet cafe?'
---
<h3 id="limited-feature-">Limited Feature:</h3>
<p>Whatsappweb feature allows us to use our whatsapp account synced with our mobile on desktop. Well it has good advantages, it allows us to text while we are working on laptop, transfer files from system to whatsapp or vise-versa etc. It is considered good if used in our personal laptop/desktop. If the same is used in Internet cafe? Then? still secure? May be not all the time.</p>
<h3 id="live-scenario-">Live Scenario:</h3>
<p>When I visited a internet cafe to get a printout, I found  a person(consider as person A for instance) who synced his whatsapp  account to one  of the systems in the cafe to  get a print of a document shared in whatsapp. The procedure he followed is as follows:</p>
<ul>
<li>He asked the internet guy (consider as person B for instance) to give a printout as person A is rushing. person B entered whatsappweb in the browser took person A&#39;s mobile to scan the qr code, person A handed his mobile over to the cafe guy. The option while scanning the qr is as below.</li>
</ul>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1562389390530/QRbisxiWh.png" alt="whatsapp1.png"></p>
<ul>
<li><p>You can observe a checkbox ticked by default stating keep me signed in neither the person B unchecked it nor this person A asked him to unchecked the option. well, the whatsapp account finally got synced. The required document was printed. Now person B has closed his browser as the work is done, and person A who got his mobile back has closed the app and left. The point to notice is the mobile data is still on.</p>
</li>
<li><p>We are having two scenarios. As the synced account is not logged out. If we open back the whatsappweb in the browser we will be still able to the access the whatsapp account without any authentication such as qr code scan as the user has checked the option keep me signed in at the time of qr scan.</p>
</li>
<li><p>And the second scenario is how long can person B pertain the session of person A&#39;s account? As long as the mobile device has internet connectivity.</p>
</li>
<li><p>Every digital service, application comes with good advantage and make our life easier but it is our responsibility to use it carefully.</p>
</li>
</ul>
<h3 id="recommendation-">Recommendation:</h3>
<p>If we consider the above scenario using it carefully is the one good solution. It is always good to ensure that you had not checked the keep me signed in option in any public or shared systems. It is suggested to logout from your accounts after use. In whatsapp web we can find a logout option on the page itself and always logout of synced systems from your mobile to ensure no session remains forever.</p>
<p><strong>Desktop logout option:</strong>
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1562389626276/6KhCV4Bcd.png" alt="whatsapp2.png">
<strong>Mobile logout option:</strong>
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1562389636156/IG4C_2MRF.png" alt="whatsapp3.png"></p>
