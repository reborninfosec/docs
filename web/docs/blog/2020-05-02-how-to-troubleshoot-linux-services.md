---
layout: post
title: 'How to troubleshoot linux services?'
---
<h2 id="linux-services-">Linux services:</h2>
<p>Linux service is a program that runs continuously in the background and serves specific application all the time.
We see lot of errors when starting Linux services, something like below
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588446267610/HP85fSyJ8.png" alt="serv1.png"></p>
<p>For example let us take samba service has the above issue and hit the below command for extra details.
<code>$systemctl status smbd.service</code></p>
<p>This will show the following.
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588446281449/Bg1-y84rW.png" alt="serv2.png"></p>
<p>This command is just showing there is error while starting the service.
To get full picture of why service is not running we have to analyze the &quot;.service&quot; file.</p>
<p>Now the samba service file is at <code>/lib/systemd/system/smbd.service</code></p>
<p>Cat the contents of the file(Truncated result):</p>
<pre><code><span class="hljs-section">[Service]</span>
<span class="hljs-attr">EnvironmentFile</span>=-/etc/default/samba
<span class="hljs-attr">ExecStart</span>=/usr/sbin/smbd --foreground --<span class="hljs-literal">no</span>-process-group <span class="hljs-variable">$SMBDOPTIONS</span>
</code></pre><p>It is showing that the binary file called &quot;smbd&quot; will have to start with the SMBDOPTIONS. Here the main program which is running continuously as service is &quot;smbd&quot;.</p>
<p>To troubleshoot the service we will directly run the same &quot;smbd&quot; binary.</p>
<p>The binary is running with the environment variables. If the values in the environment file are not accessible or corrupted then the binary throws an error. That error would be the root cause for the issue and fixing that error will make that service up and running.</p>
