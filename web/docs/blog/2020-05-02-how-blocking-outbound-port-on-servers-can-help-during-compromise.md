---
layout: post
title: 'How blocking outbound port on servers can help during compromise?'
---
<p>All these days we are implementing default rule in server firewall to block all the incoming ports during hardening process. Done? Does our servers are really hardened now? Ok let&#39;s take a scenario what if an agent is running in the compromised server sends the data to the external server using some lame http protocol?</p>
<p>It&#39;s time to think about the outgoing port. By default if a http server runs on port 80 and the connecting client can choose any port from 1024-65535. Say if a malware is sending some data through email then it occupies any of these port. We get answers automatically if nailed down these points. We have to re assess the strategy whether we really need outbound connection from that server. If our server is serving web pages then it opens incoming port 80 and except opening port 80 we don&#39;t need any extra port not even any outbound port. It&#39;s not about the well-known ports but also non reserved ports and finally this way we can harden our servers.</p>
