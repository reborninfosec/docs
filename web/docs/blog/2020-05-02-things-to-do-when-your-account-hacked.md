---
layout: post
title: 'Things to do when your account hacked'
---
<p>All our accounts are at risk most of the time. The moment we notice suspicious events in the accounts page we should be quick to respond. If we find our account is being accessed by other person then it can be treated as hacked. Now it would be better if we follow the following set of instructions.</p>
<ul>
<li>Change the password and add extra layer security such as multi-factor authentication</li>
<li>Inform to the site owner that account has been hacked</li>
<li>If you are from enterprise then initiate an investigation and digital forensics.</li>
<li><p>Request all the access logs from account service provider</p>
</li>
<li><p>The important thing is to recollect where all the places you entered the password or what all the other sites you have the same username/password combination</p>
</li>
<li><p>If you have the same credentials in other applications, check them whether they are compromised</p>
</li>
<li><p>Check your system is effected with a malware or not</p>
</li>
<li><p>Try to scan with anti-virus and update the anti-virus db</p>
</li>
<li><p>Take the data backup and re-install the operating system as a benefit of doubt</p>
</li>
<li><p>Finally accept the truth that you are the responsible for the attack and maintain proper security measures to overcome the other attacks.</p>
</li>
</ul>
