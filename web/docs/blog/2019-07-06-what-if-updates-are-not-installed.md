---
layout: post
title: 'What if updates are not installed?'
---
<h3 id="updates-are-always-seen-">Updates are always seen:</h3>
<p>Is it always necessary to install updates on our server/computer/mobile/software? Well we can notice that we get a notification prompting 22 updates are pending for approval and the similar thing in the case of a computer or even a software. As per the options set in the computer, the system directly downloads the updates and later prompts to install them. Now few software are directly installing without even prompting. Why? Why are they insisting?</p>
<h3 id="scenario-">Scenario:</h3>
<p>Recently I was working on web applications and the identified technology in use is drupal. After identifying technology, the next step is to see which version they are using. None of them were the latest released versions.
We can find any drupal site easily using google search:
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1562397319214/7zFTYpEMB.png" alt="drupal1.png">
Drupal if misconfigured gives version details by accessing &quot;CHANGELOG.txt&quot; file. 
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1562397335753/eh6ZZ0L15.png" alt="drupal2.png">
So, now we can easily list out the open vulnerabilities with respect to that particular versions., this can be done manually by checking out the cve database and can be implemented by the automated scanners. Well now that we have the list of known vulnerabilities and can start exploitation of each vulnerability with all the possible ways., i.e. A customized python scripts or by simply using metasploit (the exploitation framework). Yes the drupal vendors already released patches for the version that  is used, so patched systems can be secure from known vulnerabilities. It is the site owners responsibility to update patches as vendor can&#39;t install automatically because it can be portrayed as back door.</p>
<h3 id="similarly-">Similarly:</h3>
<p>Now this is same for your operating systems if you are a windows user or linux user, you might notice bundle of updates released for the OS and even for the services that you are running. Any small service which is not updated may even become source of exploitation. It is always suggested to install the updates and patch the systems before they get exploited.</p>
