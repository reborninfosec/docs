---
layout: post
title: 'Google zero trust overview: Identity-Aware proxy'
---
<p>Before diving into Google IAP I would like to start with google BeyondCorp. BeyondCorp is a spin-up from google to Implement zero-trust networking in the enterprise. This would eliminate the traditional VPN&#39;s and grants the access to the services based on user Identity. Coming to Google IAP, it is the implementation framework for Zero trust network. Figure of Google IAP as follows..
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588447370460/FRqkWkBeF.png" alt="googleiap.PNG"></p>
<p>Source: Google.com</p>
<p>The flow of access goes as follows:</p>
<ul>
<li>IAP proxy is configured with load balancer which works as a request handler.</li>
<li>Each request to the service passes through the proxy</li>
<li>Proxy will check whether the user has the access to the requested resource or not.</li>
<li>Then it will allow or deny the user to connect to that service.</li>
</ul>
<p><strong>Demonstration(Service running inside Google Compute instance):</strong></p>
<h3 id="prerequisites-">Prerequisites:</h3>
<ul>
<li>Google cloud service created from Instance group</li>
<li>Load balancer configured for back-end service</li>
<li>Health check</li>
<li>Firewall rule</li>
<li>IAP access</li>
</ul>
<h3 id="cloud-service-">Cloud service:</h3>
<p>Create a cloud service from instance group as follows: We need instance group because the load balancer will be only configured to instance from group.
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588447552806/xk09CUt8z.png" alt="Instancecreation.PNG"></p>
<p>Create like this.
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588447585515/I_AHNqjPi.png" alt="Instancecreatd.PNG"></p>
<p>Navigate to Network Services&gt; Load balancer&gt;New to create Load balancer.
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588447610777/iCyJniqdo.png" alt="HTTPSLB.PNG"></p>
<p>Select the HTTPS Load Balancing and configure.
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588447634422/h9gGMox0b.png" alt="Configurelb.PNG"></p>
<p>Mostly we can keep those defaults and select create.
We need to note the front-end IP and configure the same IP in the DNS settings.</p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588447665199/foafQ2WC8.png" alt="LBconfigured.PNG"></p>
<p>Configuring Firewall: VPC Network&gt; Firewall Rules:</p>
<p>Disable the default allow rule and add rule to allow traffic from LB IP address(In my case mentioned below). With this rule we are telling firewall to block all the packets and allow only from load balancer. Create a certificate using Letsencrypt:</p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588447687599/DGpOdcY9d.png" alt="Firewall.PNG"></p>
<p>Creating a IAP Proxy: Security&gt; Identity-Aware proxy: </p>
<p>This screen will automatically display the load balancer and gives green check if configured properly. </p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588447712134/WqZahzHDz.png" alt="IAP.PNG"></p>
<p>Enabling IAP is easy: Toggle IAP to ON and Tick on the backend-service to allow users for the service.</p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588447730663/rQ0qS9RkT.png" alt="IAP_access.PNG"></p>
<p>Finally the application can be accessible only to authorized users mentioned in the IAP-Secured Webapp user.</p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588447745501/WKmxTm1X1.png" alt="accessasking.PNG"></p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588447769418/Baw_kkFNz.png" alt="final.PNG"></p>
<p>If user is valid then it will permit him to access....</p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588447792591/W_o3RAjNo.png" alt="Accesses.PNG"></p>
