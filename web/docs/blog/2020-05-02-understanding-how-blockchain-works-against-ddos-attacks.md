---
layout: post
title: 'Understanding how Blockchain works against DDos attacks'
---
<h3 id="tl-dr-">TL:DR;</h3>
<p>As, all the nodes are holding the same data, only one of them cannot be a target for DDos. Because if any of the node is targeted and a DDos attack is performed against that node, data will still be available at many such other nodes, the below mentioned implementation can turn to be a solution against DDos attacks, as DDos cannot be performed on all the nodes.</p>
<h2 id="ddos-">DDos:</h2>
<p>A DDos attack is a kind of attack where attacker generates huge amount of traffic towards the target server to make server unavailable. DDos cannot breach any data but stops the transactions on that particular time. Downtime of a service also create loss to the business. OK, let&#39;s take a scenario: I am doing some transaction, say I am planning to send some money to other person account. Now what if a DDos attack has taken place at the recipient bank server soon after I click on send button? First thing is money will be debited from my account., Second thing is that the sender&#39;s bank sends request to the recipient bank to credit that money. Here the second condition fails because the recipient bank servers are experiencing DDos attack. When the bank restarts the services we may or may not get the money deducted because the transaction failed when data is at motion. Banks cannot send money again because it may lead to duplication  in the other bank.</p>
<h2 id="blockchain-">Blockchain:</h2>
<p>Let&#39;s see how Blockchain works in a simple way.. If we are doing any transaction in a Blockchain network the first thing that happens is that It debits from your account and credits with the name of other account in our local Blockchain node/database. Then it broadcasts the transaction to all the nodes and other nodes, bundle multiple transactions as a single block that will be updated into the chain.</p>
<h2 id="protection-">Protection:</h2>
<p>Yeah, here transactions are happening in a peer to peer manner between two nodes. Nodes broadcast the data to the chain after they receive. This makes the same data to stay at multiple nodes. This is called data redundancy in security terms. Redundancy keeps us secure from DDos attacks.</p>
