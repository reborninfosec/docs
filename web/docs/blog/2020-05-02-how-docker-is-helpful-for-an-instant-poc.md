---
layout: post
title: 'How docker is helpful  for an instant poc?'
---
<p>Its been more than 2 years now I am working on docker and I was addicted to it. Whatever requirement I get I will just google &quot;&lt;solution&gt; docker image&quot;, the solution can be anything built on python, java, go, perl etc.
To quickly get start with something, I will get the docker image and start prototyping with it.</p>
<h3 id="why-docker-">Why docker?</h3>
<p>Say we are running ubuntu in our system and we got a requirement that needs centos(rpm based) platform. We cannot blindly go and buy the redhat subscription or so. Rather we can write our initial piece of code then test it inside docker container. If our code works then well and good, if doesn&#39;t works then we should find a way to fix it. But setting up a centos container hardly takes 2 minutes. Building docker doesn&#39;t even need professional experience and there will be only 3 or 4 commands to spin up a container with an existing image.</p>
<p>We should eventually make a habit of writing Dockerfile files to every project we code. I expect, in the coming years everything is built for microservices  architectures. We just need to maintain a Dockerfile in the root directory of our repositories, Done and if we have a simple .yml/.yaml file then we don&#39;t even need to think about deployment. Everything can be taken care by CI/CD tools like travis, gitlab etc. I am sure that in future docker would be one of the needed skill set for developer job role and it&#39;s good to have some exposure with it.</p>
<p><strong>Note:</strong> This article is written with scope of client-server, web based application models and not with the standalone monolith based implementations.</p>
