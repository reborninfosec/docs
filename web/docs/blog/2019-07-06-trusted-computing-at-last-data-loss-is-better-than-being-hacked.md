---
layout: post
title: 'Trusted computing: At last data loss is better than being hacked!'
---
<h3 id="it-s-trusted-">It&#39;s trusted:</h3>
<p>Trusted computing is considered as one of the strongest option  among the security alternatives available. Well now that we are having a scenario where  security is provided in the form of a software or we can say it as software based security. As software is prone to attacks and nothing is full proof when it comes to software. It has the most possibilities of getting tampered. Data encryption, secure storage what not when it comes to data any software which is said to be protecting our data is firstly protecting itself from being attacked, and then protects the data. How long does it holds our data securely. We can say till the protecting software becomes a victim?</p>
<h3 id="principle-">Principle:</h3>
<p>So What&#39;s next, Software based, may be vulnerable, or may get tampered then? As, software is done with its part lets shoot at the hardware now. Here comes the trusted platform module. Well a hardware based security solution, Now  that we are protecting our &quot;important data&quot; from the hardware level. Let&#39;s see how it protect, here private key/cert is integrated inside the hardware chip. Using the chip entire data is Encrypted and PKI based authentication happens with trusted computing enabled servers. well this is all what I can understand from its working principle.</p>
<h3 id="tamper-proof-">Tamper proof:</h3>
<p>Now if an attacker attempt to steal the data, as the authentication is done through the hardware and hence get failed, as bypassing the hardware module is not possible. Then hence it is all the trusted chip protecting the data. Now if someone try to tamper the chip then the data inside the persistent storage will be lost .Now the attacker is holding the data but not the access of the data.</p>
<h3 id="choose-wisely-">Choose wisely:</h3>
<p>Although the data may be saved from getting accessed by the attacker but nothing can stop the loss of data if tried to tamper. Choose which is better whether to lose your data or to keep your data live and allow others to access.</p>
<p><strong>Note:</strong> The above mentioned data is best to my understanding and doesn&#39;t represent any criticism</p>
