---
layout: post
title: 'Keep yourself Up-to-date because hackers are!'
---
<h2 id="what-makes-us-up-to-date-">What makes us up-to date?</h2>
<p>The moment we have awareness of any event, social announcement, information etc.. then we are up-to date with the market.</p>
<h2 id="why-should-we-">Why should we?</h2>
<p>Consider stock market trading. All the trading happens based on the latest events,announcements happened in a company. Similarly this will also applies to our digital assets.</p>
<h2 id="scenario-">Scenario:</h2>
<p>Late in 2016, Indian government has announced that they have cancelled the 500/1000 Rs notes in-order known as &#39;demonetization&#39;. Hackers from other country took this news as an advantage and sent mails to our customers by spoofing our corporate mail id&#39;s. The mail says like this: &quot; Dear Sir, Due to the inconsistent financial situation in India, our government is tracking every transaction for auditing purposes. We request to kindly send the payment to our offshore account in Hong Kong. Account details are as follows......&quot;. This email seems to be legit and crafted perfectly based on societal situation. Our customers believed this as an alert mail and sent our payments to the bank without any acknowledgement mail to us. Our organisation may not be the only victim.There may be other 1000 companies effected by this crafted phishing attacks.</p>
<h2 id="the-weapon-">The Weapon:</h2>
<p>The only weapon attackers used is the news that happens at the moment. They created a situation in the mail and made our customers believe that it really happened. We can also use this weapon to stay secure. One can interpret the data in a way they want. As we are living in a society where information is treated as weapon, we should always update our knowledge on whats happening and interpret the resources in a way how we think as well as how attacker is thinking.</p>
<h2 id="update1-big-billion-day-">Update1: Big billion day!</h2>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1562286806424/_3vhZbGOs.jpeg" alt="bigbillion.jpg"></p>
