---
layout: post
title: 'If you really want it, you wont loose it!'
---
<h2 id="what-everyone-need-">What everyone Need?</h2>
<p>Yeah Money, everyone need money. We use money for buying something. People keep money in physical wallets/pouches to keep handy and safe.</p>
<h2 id="audit-">Audit:</h2>
<p>While traveling in a busy bus or when we are going out, the first thing we do is check whether we are carrying our wallet or not. If carrying next thing we do is check for money. We will keep checking our wallets frequently till we reach our home because we feel like it is an important asset. In simple words the process of frequently checking our wallets is known as Audit. We do audit for our physical assets but, are we really do audits for our digital assets? May be at very rare times.</p>
<h2 id="audit-every-digital-asset-">Audit every digital asset:</h2>
<p>OK lets discuss what is a digital asset. Simply saying, any electronic data which has some value is called a digital asset. Some of the digital assets are Our Internet banking account, email accounts, digital wallets, shared storage etc.. We need to audit these digital assets frequently because if we loose control on our digital assets then it will cost more than the money that exists in our physical wallet.</p>
<p>My philosophy in living is &quot;If you really want it, you won&#39;t loose it&quot;. If we really believe our digital wallets are so important then we wont loose them. In digital terms loosing is nothing but getting hacked. Now its time for everyone to asses their digital assets whether they are important or not.</p>
<p>If yes then audit every asset like</p>
<ul>
<li>systems - run an antivirus</li>
<li>emails - check account activity</li>
<li>online banking - transactions</li>
<li>digital footprints - google your full name(Mandatory)</li>
<li>social - keep an eye on your friends activity and on OAuth access.....</li>
</ul>
<p>This is to make sure that what exactly is you in the digital society.</p>
