---
layout: post
title: 'How helpful is pastebin for security professionals?'
---
<h2 id="pastebin-">Pastebin:</h2>
<p>We might have heard about this online text storage platform. Simply saying pastebin is an online application that allows any user to paste the text using API and web. It became popular these days because most of the anonymous content is available in it. The data in pastebin includes passwords, tokens, personal details etc. Pastebin started their own way to delete these stuff if someone uploads.</p>
<h2 id="scraper-api-">Scraper API:</h2>
<p>Using the pastebin scraper API we can get the pastes using an API call. Scraper API needs Pro subscription and we need to explicitly specify our IP address because only one IP address can be used for one pro account.</p>
<h2 id="pastebin-for-security-">Pastebin for security:</h2>
<h3 id="red-team-">Red team:</h3>
<p>As a red team person we can find some sensitive data of our organization in the pastebin search. </p>
<h3 id="blue-team-">Blue team:</h3>
<p>As a blue team person we can do lot of things, below are few of them..</p>
<ul>
<li>Scrape all the data using API to check malware posted data. We can check the frequency with which these pastes are uploaded by malware.</li>
<li>Dump the passwords if we find any and add them to your organization&#39;s breached password database so that you should not allow your users to use that password.</li>
<li>Alert government and law enforcement agencies related to piracy data(I have seen people keep torrent/magnet links and we need to sink hole those URLs)</li>
<li>We can implement threat intelligence based on some of the data like IP address,  gps coordinates etc..</li>
<li>We can find the tor links for detecting crime related stuff.</li>
</ul>
<p>These are very few and we can even do more, we have written a code to find emails in a single scrape. Follow the GitHub link and we are working on trying continuous scraping and adding persistent storage in mongodb.</p>
