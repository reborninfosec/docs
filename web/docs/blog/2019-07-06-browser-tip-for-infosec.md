---
layout: post
title: 'Browser tip for Infosec!'
---
<h3 id="chrome-vs-firefox-">Chrome vs Firefox:</h3>
<p>Chrome and Firefox are the two most popular browsers in the market. Chrome is maintained by Google and Firefox is maintained by Mozilla foundation.</p>
<h3 id="hidden-login-in-chrome-">Hidden login in chrome:</h3>
<p>The moment we install chrome it will ask for login into Google. It is not for gmail login but keep you logged in always in the browser. Chrome will sync our entire activity with that account(Unless activity is paused in Google myaccount). And secondly while configuring proxy(burp or zap)  for web penetration testing in chrome, it will change the system values rather than browser only values which is not a good option.</p>
<h3 id="right-option-">Right option:</h3>
<p>Choosing firefox as the main browser is the ideal solution. Although we are using firefox for default browsing, we also need to setup proxy(burp or zap) to intercept the requests in the browser. We can&#39;t use Default browser as proxy browser as it generate lot of legitimate traffic which we don&#39;t need during analysis.</p>
<h3 id="2nd-browser-">2nd Browser:</h3>
<p>We need to isolate the proxy traffic from default traffic. For that we can use the other browsers in the market which are derived from Firefox eg:Waterfox. The first advantage is we use Firefox for default browsing and it is important because we do lot of goggling during the pen-testing and we don&#39;t want to capture that traffic. Secondly because waterfox browser is forked version of Firefox, the proxy settings are used for browser wide not system wide.</p>
