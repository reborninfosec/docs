---
layout: post
title: 'Third party threat!'
---
<h3 id="illustration-">Illustration:</h3>
<p>A company named &quot;example pvt ltd&quot; has outsourced their development/Infra(IT) to third party vendors eg &quot;example consultancy&quot;. If an attacker want to hack &quot;example ltd&quot; then he could enter through the &quot;example consultancy&quot;.</p>
<h3 id="reasoning-">Reasoning:</h3>
<p>As we can see an attacker can enter through third party organizations , we need to re-assess our organization policies with respect to third party vendor verifications. Consider this real time incident: One of the Indian prestigious organization(Manufacturing) has opened bids for the &quot;supplying of PLC&quot; to their motors. If an attacker want to hack this company, then they can bid very low price to get that tender. Now the tender scope says that &quot;Vendor should also give computer along with the electrical material&quot;. This is a serious issue. Any company can insert some hardware chip insider computer which can send entire data to their servers. May be the prestigious organization has secured their computers and networks but we don&#39;t know about the third party organizations. Finally the prestigious billion dollar organization can be compromised by hacking their third party vendors and entering through it. Similar example showcased in &quot;Tom Clancy&#39;s Jack Ryan&quot; where military people allowed third party vendors to import dead bodies, an attacker enters into military base as a dead body and exploit inside to rescue their leader.</p>
<h3 id="conclusion-">Conclusion:</h3>
<p>It&#39;s always important to remember that trust cannot assure security. Security needs proper assessments such as creating isolated network, giving less privileges, auditing every communication and even verifying the employees who work behalf of all third party vendors. </p>
