---
layout: post
title: 'How to build your own enterprise grade Firewall/Router with less than $20'
---
<p>We are taking <strong>Pfsense </strong> as a firewall version. Through out this article all the requirements and calculations are based on pfsense.</p>
<p>List of things needed to build an enterprise firewall:</p>
<ul>
<li>CPU with Multi core processor</li>
<li>Memory</li>
<li>Storage</li>
<li>Firewall Software</li>
<li>Multiple network cords</li>
</ul>
<h2 id="assembling-">Assembling:</h2>
<h3 id="hardware-">Hardware:</h3>
<p>Hardware can be any PC&#39;s motherboard which is residing in your gars which is having minimum 512GB RAM with 1 Core CPU. We can assume that even systems in gars will also have 2Gigs of memory with dual core. Go through the picture below to check official pfsense requirements.
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588439559902/FZHnKQAbN.png" alt="pfsense_hardware.png">
This hardware is the key part to build a pfsense firewall because we will definitely have old systems in our gars. We are not going to buy the motherboards, RAMs and HDDs but instead we will use our own old hardware.</p>
<h3 id="nic-s-">NIC&#39;s:</h3>
<p>Basically any router or gateway should have 2 NIC&#39;s. One for Intranet and the other for Internet. For every motherboard in a computer we mostly have only single NIC. To add more NIC&#39;s we can use the PCI/PICE slots on a motherboard.
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588439666217/lb9FN-tt5.jpeg" alt="220px-Supermicro_AOC-SGP-I2_Gigabit_Ethernet_NIC,_PCI-Express_x4_card.jpg"></p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588439942962/I6Tpb49Ug.jpeg" alt="1200px-PCI_Slots_Digon3.jpg">
A Peripheral Component Interconnect (PCI) slot is an open portion on a motherboard where we can use it to install additional hardware like NIC, RAID, Graphic cards etc. The only hardware we are purchasing which costs around $10 is a Gigabit Ethernet card. We will buy 2 numbers to create enterprise fail over which will be discussed later in the article.</p>
<h3 id="pfsense-">Pfsense:</h3>
<p>Pfsense is an opensource firewall/gateway operating system developed on top of FreeBSD. We can install pfsense into our hardware as a regular operating system. Follow the documentation from pfsense to know the process of installation.  <a target='_blank' rel='noopener noreferrer'  href="Pfsense installation">https://www.netgate.com/docs/pfsense/book/preface/index.html</a> </p>
<p>Advantages:</p>
<ul>
<li>Opensource(Free)</li>
<li>Firewall</li>
<li>Router</li>
<li>VPN</li>
<li>Load Balancer and much more!</li>
</ul>
<h3 id="enterprise-ready-">Enterprise Ready:</h3>
<p>For regular needs installing a pfsense with two NIC&#39;s is enough. But, for an enterprise we need things like fail-over, load balancing, High availability etc.. Fail-over is a concept used in computers which states that if one service is failed then there will be other service serving the same content. Here we had three NIC&#39;s in which one is used for LAN and the other two are used for WAN. Follow the link for pfsense configuration instructions  <a target='_blank' rel='noopener noreferrer'  href="Dual wan instructions">https://www.netgate.com/docs/pfsense/routing/multi-wan.html</a> .</p>
<p>I accept that there will be difference between commercial firewalls and opensource firewalls. This article is for the organizations/personals who doesn&#39;t have enough budget to spend for commercial solutions or for the personals who have the capability to customize opensource and would make their own commercial firewall.</p>
