---
layout: post
title: 'What is a strong PASSWORD?'
---
# What is a Password?

<p>Assume a house has a lock to restrict strangers from entering the house. Anyone who is having the key can enter the house. In the same way any web/desktop application requires a password to give access to a application. Anyone who knows the password can enter into the application and it may not be the legitimate user always.</p>
# How passwords are stored in DB?
<p>Generally all the passwords are stored in DB using a cryptographic function called hashing. A hash function simply converts variable length text into alpha numeric text with fixed length. Example, hashing algorithms are sha1, sha256, md5.. etc. This is how it works.When a user inputs his user-name and password, the web application converts that string into hash value using any one of the algorithm. Then passes that hash to database to store. When user login into the application, credentials are converted into hashes, matches the value with the one stored in the database and authorize the user.</p>
# Password cracking:
<p>If a website has a vulnerability, any hacker can run arbitrary code into that application and then dumps all the password hashes into his local storage. Consider if a password has 4 letter numeric value then the number of possible permutations with repetitions are</p>
<p>P(n,r) Permutations(10,4) = 10 power 4 = 10000</p>
<p>Which means that all the ten numerics are selected and arranged 4 each at once. So total number of 4 digit passwords are 10000. A computer which can hold good processing power i.e number of threads, uses each thread to convert all the numbers into hashes on parallel. Which means to covert 10000 values to hashes using dual core processor it takes not more than 1 minute. Now lets save this 10000 words as a dictionary.</p>
<p>Now the attacker takes all the dumped hashes and write a code to compare each hash with the 10000 word dictionary which again takes less than a minute to execute. Finally if he found any hashes matching with the dictionary, then the password is said to be cracked.</p>
# What is a strong Password.?

<p>Now you are clear that cracking a 4 digit numeric password is easy. Let&#39;s think about what is a good strong password. Any suggestions? let me ask you one more time &quot;Whatisagoodstrongpassword?&quot; (Yeah exactly that string is OK to consider as a strong password). While choosing passwords just keep in mind that there are lot of GPU&#39;s in the market which can iterate 10000 strings in 1 sec. So the password &quot;Whatisagoodstrongpassword?&quot; has length of 26 characters. </p>
<p>So the dictionary needs to crack this password is derived as follows:</p>
<p>Total =  26+26+10+33 (Alphabets small &amp; capital , numbers, special characters)</p>
<p>Then total permutations(password dictionary) = 3.45981e+51 ... </p>
<p>Look at the number! for storing, parsing these many values can be done only on super computers. So in what time you think this password can be cracked? In Days? Months? Years? or a Decade. </p>
# Misc:
While considering password make sure you consider the maximum length with minimum complexity and easy to remember. Henceforth there is no secure website in the world which cannot be breached. But the strong passwords can always save us when the website is breached and it is has your password hash. Maintaining different passwords for different portals can also save us from known password attacks.</p>
