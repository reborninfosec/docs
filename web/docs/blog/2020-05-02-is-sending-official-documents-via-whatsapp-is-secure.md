---
layout: post
title: 'Is sending official documents via WhatsApp is secure?'
---
<p>&quot;Sir please send your documents through WhatsApp we will process the remaining&quot; This is what I was asked when applying for my credit card. Well this process will get our work done but not the ideal one. Let us assume that I have sent all the documents to the service guy who is an intermediate between the customers and the bank, he will submit all the documents to the bank and apply for credit card on behalf of us. Please note that there is no trace to find whether our documents are deleted or not after use. </p>
<p>Suppose to apply a credit card in INDIA we need to submit following documents.</p>
<ul>
<li>PAN Card</li>
<li>AAdhar</li>
<li>Bank Statement</li>
<li>Rental Agreement</li>
<li>Recent pay slips etc..</li>
</ul>
<p>The above documents are considered as very personal documents which has all the data like where we live, how much money we have/spent/save(Through statement), Where we work and how much we earn etc. It is OK if we are providing to the organisation directly because organisations now a days are maintaining portals to apply online and they keep the data private within their database. But here, we are submitting these documents to a person who is from a third party organization and there is 100% possibility to sell the data to make some business out of it. </p>
<p>To be secure from these easy breaches, we have to create our own rules to not to share any such documents with any untrusted persons. </p>
