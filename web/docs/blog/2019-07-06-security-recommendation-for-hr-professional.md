---
layout: post
title: 'Security recommendation for HR professional'
---
<h3 id="why-hr-">Why HR:</h3>
<p>Human resource management is the first point of contact to the external network as they need to share email ID to public. People looking for job in that any organization will attach resume and send the same to HR via email. They attached files may be pdf, word, odf,. etc..</p>
<h3 id="actual-threat-">Actual Threat:</h3>
<p>Attacker takes this as an advantage, then crafts a word document having macros and sends to HR mail id with the name of resume. Assuming that it is an original job request, the HR person will open it. Boom! the payload inside the macro gets executed and introduces malware to that PC. That said, It&#39;s not easy to craft the macro but it is not an impossible job. Execution of payload may depends on the vulnerable office suites, pdf readers, mail clients.. etc. If the PC is running without updates and patches, then it will be very easy to exploit.</p>
<h3 id="recommendation-">Recommendation:</h3>
<ul>
<li>Adding virus scanner plugins in mail client will save from known viruses inside the document.</li>
<li>Avoid opening mails from sources like .tk,.protonmail ..etc</li>
<li>Never turn on Macros in document readers</li>
<li>Updating patches of Office,pdf suites.</li>
</ul>
