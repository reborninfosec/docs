---
layout: post
title: 'How to use letsencrypt free ssl certificate for all subdomains ?'
---
<p><strong>Letsencrypt </strong> is an organisation that issues free SSL certificate for web applications. SSL certificate is used to protect the data in motion from client-server vice-versa. If we take any website the first thing we should do is to configure ssl certificate for that website. Generating and renewing letsencrypt ssl certificate is pity much simple and can be done with very few commands. Let us consider we have an instance running in any cloud provider with apache as web server, the configuration procedure is as follows..</p>
<blockquote>
<p>Note: Always we should remember that letsencrypt needs a domain name to generate certificates. For internal applications and IP address we can use self-signed certificates.</p>
</blockquote>
<p>OS: ubuntu:18.04</p>
<p>Install Apache: </p>
<p><code>sudo apt-get install apache2</code></p>
<p>We use certbot-auto as a tool to install and renew certificates automatically.</p>
<p>Cloning from github amd run the certbot</p>
<pre><code>git <span class="hljs-built_in">clone</span> https://github.com/certbot/certbot
<span class="hljs-built_in">cd</span> certbot
</code></pre><p>Installing certificates:</p>
<p><code>./certbot-auto certonly -d example.reborninfosec.com</code></p>
<p>The above command is trying to generate certificate only for <code>example.reborninfosec.com</code> subdomain. Certificates are stored in <code>/etc/letsencrypt/live/example.reborninfosec.com/</code> directory.</p>
<p>To use the above certificate for a apache, the virtual host configuration is like below..</p>
<pre><code><span class="hljs-section">&lt;VirtualHost *:80&gt;</span>  <span class="hljs-comment">#To listen on port 80 and redirect to port 443</span>
  <span class="hljs-attribute"><span class="hljs-nomarkup">ServerName</span></span> example.reborninfosec.com
  <span class="hljs-attribute"><span class="hljs-nomarkup">RewriteEngine</span></span> <span class="hljs-literal">On</span>
  <span class="hljs-attribute"><span class="hljs-nomarkup">RewriteRule</span></span> ^/?(.*) https://<span class="hljs-variable">%{SERVER_NAME}</span>:443/<span class="hljs-number">$1</span><span class="hljs-meta"> [R,L]</span>
<span class="hljs-attribute"><span class="hljs-nomarkup">RewriteCond</span></span> <span class="hljs-variable">%{SERVER_NAME}</span> = example.reborninfosec.com

<span class="hljs-attribute"><span class="hljs-nomarkup">RewriteRule</span></span> ^ https://<span class="hljs-variable">%{SERVER_NAME}</span><span class="hljs-variable">%{REQUEST_URI}</span><span class="hljs-meta"> [END,NE,R=permanent]</span>
<span class="hljs-section">&lt;/VirtualHost&gt;</span>

<span class="hljs-section">&lt;VirtualHost *:443&gt;</span> <span class="hljs-comment">#listening in port 443 over ssl</span>
  <span class="hljs-attribute"><span class="hljs-nomarkup">ServerName</span></span> example.reborninfosec.com
  <span class="hljs-attribute"><span class="hljs-nomarkup">DocumentRoot</span></span> /var/www/example
  <span class="hljs-section">&lt;Directory "/var/www/example/"&gt;</span>
   <span class="hljs-attribute">Require</span> <span class="hljs-literal">all</span> granted
   <span class="hljs-attribute">AllowOverride</span> <span class="hljs-literal">All</span>
  <span class="hljs-section">&lt;/Directory&gt;</span>
  <span class="hljs-attribute">ProxyRequests</span> <span class="hljs-literal">off</span>

<span class="hljs-attribute">SSLCertificateFile</span> /etc/letsencrypt/live/example.reborninfosec.com/fullchain.pem #Public key for CA
<span class="hljs-attribute">SSLCertificateKeyFile</span> /etc/letsencrypt/live/example.reborninfosec.com/privkey.pem #Private key to encrypt
<span class="hljs-attribute">Include</span> /etc/letsencrypt/options-ssl-apache.conf
<span class="hljs-section">&lt;/VirtualHost&gt;</span>
</code></pre><p>Restarting the apache server results in serving https website using letsencrypt ssl certificate.</p>
<p>General errors while installing:</p>
<ul>
<li>Letsencrypt server unable to access our cloud instance - Check the firewall settings</li>
<li>Letsencrypt server unable to access our cloud instance - Check the dns settings(Ping example.reborninfosec.com should hit original server)&#39;</li>
<li>Error (cannot generate cert) - We can generate only 5 certificates a week for single domain
Letsencrypt certificate is only valid for three months. To renew it automatically we should add the letsencrypt certbot command to the crontab as follows..
<code>0 1 * * * certbot-auto renew --quiet --post-hook &quot;service apache2 restart&quot;</code>
Everyday at 1 am this will check our certificate expiry date and renews automatically.</li>
</ul>
