---
layout: post
title: 'How Open IO & n/w ports can put companies at risk'
---
<h3 id="plug-and-connect-">Plug and connect:</h3>
<p>We can see open ports(physical) in places like airports, restaurants, office lobbies and even at homes.  These ports can be a printer port, network port, display port etc. We keep them open because we want our environment to be scale and flexible. But why don&#39;t we consider that these ports are also possible threats for attack?</p>
<h3 id="plug-and-hack-">Plug and hack:</h3>
<p>Consider your organization has an open Ethernet port in the waiting hall. Basically the purpose of waiting hall is to keep guests wait there. Now an attacker can reach that organization as a guest and connects to Ethernet port with a cable. Boom, he will be inside organizations network so easily.</p>
<h3 id="recommendations-">Recommendations:</h3>
<p>Although we have secured organization by applying patches, employee awareness etc.. We should also consider the following questions:</p>
<ul>
<li>Does our open (IO&amp; n/w) ports has mac binding or any authentication mechanism.</li>
<li>Does our open (IO &amp; n/w) ports are isolated from organization main network</li>
<li>Does our open (IO &amp; n/w) ports are monitored</li>
<li>Do we have control on open (IO &amp; n/w) ports</li>
</ul>
<p>If we can prepare solutions for the above problems, then we can harden our infra from &quot;plug and hack&quot; attacks.</p>
