---
layout: post
title: 'Why we need to deactivate the unused accounts'
---
<h3 id="too-many-accounts-">Too many accounts:</h3>
<p>From the time we started using computer to till date, most of us might have created more than one email account. We must have created the first account for temporary need and later we realized that the mail id is not professional. So we create one more mail for our professional work.</p>
<h3 id="ignored-">Ignored:</h3>
<p>While we are used to use our professional mail id, we slowly lose our interest on old mail and finally ignore it. Now the problem is we have created the initial mail id with old passwords, names, details and with very less privacy settings. It&#39;s no surprise that there were no privacy settings that time. But these mail IDs are carried till date without being accessed for years( not accessed by us but accessed by hackers). Yes, hackers can target our old IDs and we don&#39;t even get an SMS alert because we may not have or may not use the same mobile number that we had that time . Another serious thing to consider is the initial mail id or any random mail id may be the backup one for the latest mail id. This is so serious that if someone get the access of our weak mail id then they can easily access the new mail id.
<strong>Only thing to remember is always deactivate the  accounts we don&#39;t use.</strong></p>
