---
layout: post
title: 'How to upload files by hiding identity'
---
<h3 id="file-metadata-">File Metadata:</h3>
<p>Every file has metadata and basically metadata is just the file information that explains about the file. Let us consider this image file and its metadata as follows:</p>
<h3 id="image-">Image:</h3>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1575784226286/VS-FsryLN.jpeg" alt="name.jpg"></p>
<p><strong>Metadata:
</strong></p>
<ul>
<li>File Name                                : name.jpg
Directory                                 : .
File Size                                   : 1692 kB
File Modification Date/Time    : 2018:10:27 22:18:53+05:30
File Access Date/Time             : 2018:10:27 23:08:17+05:30
File Inode Change Date/Time  : 2018:10:27 23:07:55+05:30
File Permissions                       : rw-r--r--
File Type                                  : JPEG
File Type Extension                 : jpg
MIME Type                              : image/jpeg
Exif Byte Order                        : Big-endian (Motorola, MM)
Modify Date                             : 2018:07:24 14:06:25
GPS Img Direction                   : 218
GPS Date Stamp                      : 2018:07:24
GPS Img Direction Ref             : Magnetic North
GPS Time Stamp                      : 08:36:23
GPS Altitude Ref                       : Unknown (2.2)
Camera Model Name                : Le X526
Y Cb Cr Positioning                   : Centered
Resolution Unit                         : inches
Y Resolution                              : 72
Software                                    : s2-user 6.0.1 IIXOSOP5801910121S 44 release-keys
Color Space                               : sRGB
Create Date                               : 2018:07:24 14:06:25
F Number                                  : 2.2
Focal Length                              : 3.7 mm
Aperture Value                           : 2.2
Exposure Mode                          : Auto
Sub Sec Time Digitized              : 109920
Exif Image Height                      : 3264
Focal Length In 35mm Format  : 4 mm
Scene Capture Type                  : Standard
Scene Type                                : Directly photographed
Sub Sec Time Original              : 109920
Exposure Program                    : Not Defined
White Balance                           : Auto
Exif Image Width                       : 2448
Sub Sec Time                             : 109920
Shutter Speed Value                  : 1/509
Metering Mode                          : Center-weighted average
Date/Time Original                    : 2018:07:24 14:06:25
Components Configuration       : Y, Cb, Cr, -
Flash                                          : Off, Did not fire
Exif Version                               : 0220
Interoperability Index                : R98 - DCF basic file (sRGB)
Interoperability Version            : 0100
Brightness Value                        : 0
ISO                                             : 100
Sensing Method                         : One-chip color area
Flashpix Version                        : 0100
Exposure Time                           : 1/509
X Resolution                               : 72
Make                                          : LeMobile
Thumbnail Length                      : 18926
Thumbnail Offset                        : 940
Compression                               : JPEG (old-style)
Image Width                                : 2448
Image Height                               : 3264
Encoding Process                        : Baseline DCT, Huffman coding
Bits Per Sample                           : 8
Color Components                       : 3
Y Cb Cr Sub Sampling                 : YCbCr4:2:0 (2 2)
Aperture                                       : 2.2
GPS Date/Time                             : 2018:07:24 08:36:23Z
Image Size                                    : 2448x3264
Megapixels                                    : 8.0
Scale Factor To 35 mm Equivalent: 1.1
Shutter Speed                                : 1/509
Create Date                                   : 2018:07:24 14:06:25.109920
Date/Time Original                        : 2018:07:24 14:06:25.109920
Modify Date                                   : 2018:07:24 14:06:25.109920
Thumbnail Image                           : (Binary data 18926 bytes, use -b option to extract)
Circle Of Confusion                        : 0.028 mm
Field Of View                                  : 154.9 deg
Focal Length                                   : 3.7 mm (35 mm equivalent: 4.0 mm)
Hyperfocal Distance                       : 0.22 m
Light Value                                      : 11.3</li>
</ul>
<p>We can get this data by using a tool called exiftool.</p>
<p>The command we have to use to get the details is:</p>
<pre><code><span class="hljs-string">'exiftool &lt;filename&gt;'</span>
</code></pre><h3 id="unnecessary-data-sharing-">Unnecessary data sharing:</h3>
<p>As we can observe, the image has lot of metadata which includes the software used,  camera model, gps data etc.. Basically we always want to share the image but not the other unnecessary details which can result in tracking us. So how can we escape from this?
Yeah, here is a trick for people like us who want to share their picture not the data.
Use the same exif tool to remove all the metadata by hitting the following command:</p>
<pre><code><span class="hljs-variable">$exiftool</span> -all= &lt;image name&gt;
</code></pre><p>eg: exiftool -all =name.jpg</p>
<p>This command will removes all the metadata and now the metadata is follows:</p>
<ul>
<li>File Name                                 : name.jpg
Directory                                  : .
File Size                                    : 1673 kB
File Modification Date/Time     : 2018:10:27 23:50:19+05:30
File Access Date/Time               : 2018:10:27 23:50:19+05:30
File Inode Change Date/Time     : 2018:10:27 23:50:19+05:30
File Permissions                         : rw-r--r--
File Type                                    : JPEG
File Type Extension                   : jpg
MIME Type                                : image/jpeg
Image Width                               : 2448
Image Height                             : 3264
Encoding Process                      : Baseline DCT, Huffman coding
Bits Per Sample                         : 8
Color Components                     : 3
Y Cb Cr Sub Sampling               : YCbCr4:2:0 (2 2)
Image Size                                 : 2448x3264
Megapixels                                 : 8.0</li>
</ul>
<p>As we can observe that there is drastic change in the metadata of that image file. The above technique can be used for all types of files and I assume this technique will give us an added privacy.</p>
<p><strong>Note:</strong> Only few websites are automatically removing the metadata while uploading but not all.</p>
