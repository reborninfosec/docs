---
layout: post
title: 'Environment setup for aspiring security professionals'
---
<h2 id="wanna-become-a-infosec-professional-">Wanna become a Infosec Professional:</h2>
<p>Are you willing to become security professional? If yes we can learn theoretical concepts from the INTERNET, but the real problem is how can we execute that knowledge. We have to be so conscious as there is a thin line between ethical and criminal. If we have legal access to perform some hack on a website/network then it is legal and If we perform any such on web/network without access then it&#39;s an offense. Offensive approach is so dangerous and can be sued very high based on country&#39;s/company&#39;s law.</p>
<h2 id="how-to-execute-">How to execute:</h2>
<p>Yeah, the only way we have is to simulate the original environment and  create the clone locally in your system. Then we should start the attack. To isolate an infrastructure we should use any of the visualization technologies like vmware, virtual box and also docker for web services.</p>
<h2 id="choosing-the-technology-">Choosing the Technology:</h2>
<p>While choosing the visualization tools we got two options. First is the open source and second is free-ware/commercial. Virtual-box (Windows,Linux,Mac),kvm libvirt(Linux) comes under opensoure and vmware player/workstation,hyperv comes under commercial/free-ware. Choosing virtual-box is most preferable as it has both cli and GUI. Even we can use Vagrant for automating the creation of virtual infra.</p>
<h2 id="setup-">Setup:</h2>
<p>Generally after creating the virtual instances just create one private network using host-only driver in virtual box as below.
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1561821401968/UU8wbTFaX.png" alt="virtualbox1.png">
Then configure the host IP address details followed by DHCP leases.
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1561821410735/g9-pY5Zqi.png" alt="vbox2.png">
These IP address ranges are automatically assigned to the guest virtual machines.
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1561821418585/5SqJqsa9N.png" alt="vbox3.png"></p>
<h2 id="topology-">Topology:</h2>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1561821425421/p3s1T3iaY.png" alt="vbox4.png">
The above environment will have two network interfaces i.e physical and virtual. Host PC will have access to Internet from physical NIC and has access to VM from Virtual NIC. Use this topology for getting started with hacking/exploitation. Going further we can also create complex network infra using virtual box which have entities like corporate network, home network, VPN, Firewall using a single Vagrant file.</p>
