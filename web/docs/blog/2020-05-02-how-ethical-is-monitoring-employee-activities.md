---
layout: post
title: 'How ethical is monitoring employee activities?'
---
<p>Now a days big debate is happening on data collection topic. People say employers are collecting data of employees which is not fair and it is becoming the privacy violation. There is a thin line between the Employee data and the personal data.</p>
<h2 id="personal-data-">Personal data:</h2>
<p>Personal data collection includes data which define us from various aspects. Some of them are &quot;What hero we like?, what item we eat?, For whom we vote in this elections? ...etc.&quot; These are mostly used in market research and drives the business of others. Personal data of &quot;Alice&quot; consists of her birthday, father, mother, occupation, taste, interest, salary etc..</p>
<h2 id="employee-data-">Employee data:</h2>
<p>Organizations implement employee monitoring to secure their assets from internal data breach. Basically employers do traffic analysis and collect the logs. It&#39;s not actually the employee data rather it is a machine and identity data. Organizations won&#39;t monitor the payload. Payload is the actual data employee transmit. They deploy agents into all their IT Assets and monitors these assets. The main aim to monitor is to derive Which files are being accessed at which time, How long employee is spending on Internet, how many conversations employee has with the external accounts etc.. The monitoring doesn&#39;t include any personal information of an employee as mentioned in the personal data. If an organization find any anomaly in the traffic, then they will initiate the forensic triage to find out the malicious activity in their IT network.</p>
