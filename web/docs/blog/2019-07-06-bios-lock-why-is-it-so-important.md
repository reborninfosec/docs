---
layout: post
title: 'Bios lock. Why is it so important?'
---
<h3 id="my-pc-is-secure-">My PC is secure!</h3>
<p>Because I have</p>
<ul>
<li>Strong password</li>
<li>Anti virus</li>
<li>Encrypted files</li>
<li>OS with patches updated
But bad news , the above list may not save me because my bios has no password.</li>
</ul>
<h3 id="what-can-happen-">What can happen?</h3>
<p>An operating system is just a software which runs on top of hardware. If an attacker can boot other software(Live OS) then he can see all the resources of other software(The default OS). It will become even worse as it won&#39;t store any logs which makes impossible to prove it forensically.</p>
<h3 id="what-s-next-">What&#39;s next:</h3>
<ul>
<li>Firstly create a password for your bios and enable it. This will make your PC 99% secure unless someone flash motherboard ROM.</li>
<li>Checkout trusted computing technology and find a way to implement it.</li>
<li>Physically secure your laptop/pc by locking the hardware so that no one can access the cmos/motherboard.</li>
</ul>
