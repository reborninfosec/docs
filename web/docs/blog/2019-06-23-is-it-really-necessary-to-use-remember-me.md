---
layout: post
title: 'Is it really necessary to  use "REMEMBER ME"?'
---
<h2 id="no-i-can-t-remember-">No I can&#39;t remember:</h2>
<p>Is it  really hard to remember credentials? Or Is it hard to type the credentials each time you login into a account? Or Whether we don&#39;t find time to give enter credentials and login or we don&#39;t feel it is necessary to enter credentials each time we login?</p>
<h2 id="it-s-not-remembering-it-s-just-saving-">It&#39;s not remembering, It&#39;s just saving:</h2>
<p>Well what ever may be the reason most of us check the remember me checkbox when we login into a account. So What? when you check this option your username and password get saved to your computer, So when ever you visit the site the credentials are kept entered and it takes just a click to login not more than that. We can even find this passwords in the saved passwords option of your computer browser.Yes it saves your time and the necessity of  typing the credentials each time you visit the site  if it is not a shared computer. What if it is a shared computer? different users use a same computer? then? It has a very simple answer anyone who is accessing your computer and visited the site for which the credentials are saved  can login into your account just by using a click like how you do.One can access your account and image if it contains some important data what can happen. Or any malware can access the file(generally .SQLite) and steal the credentials.</p>
<h2 id="remember-">Remember:</h2>
<p>Use remember me if you are sure that no one/malware is accessing your system OR ELSE try remembering the username and password. Enter the credentials each time you login and clear the saved passwords often from your browser if any. There are some online/offline vaults/password management tools where you can save passwords. But, the master password should be kept secret/secure and strong enough.</p>
