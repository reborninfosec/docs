---
layout: post
title: 'Next Next Install'
---
<h3 id="any-software-">Any Software:</h3>
<p>When I ask someone &quot;How do I install this software?&quot; then the answer I always get is &quot;next next install&quot;. So easy to install right.You can take any software(At-least in Windows), this super easy solution works.</p>
<h3 id="why-">Why:</h3>
<p>Why I&#39;m not going to customize the software while installing? May be it will take 20 more seconds which I don&#39;t have! right.. And it&#39;s also because I have a fear that software may not function properly if I change the default values.</p>
<h3 id="what-is-happening-">What is happening:</h3>
<p>When you install software, you are asked to accept the terms and conditions. The acceptance is part of Law Model called &quot;Take it or leave it&quot;. Which means that all the terms and conditions of software are clearly written and awaits for your acceptance. If you are not OK with the T&amp;C then you can exit installing software. General terms includes..</p>
<ul>
<li><p>The data that vendor collect</p>
</li>
<li><p>The Policies that vendor had</p>
</li>
<li><p>Expire if any</p>
</li>
<li><p>License if any</p>
</li>
</ul>
<p>OK Now you have accepted all T&amp;C now i have to customize the installation. Why? Because i have read all the terms and conditions it is clearly mentioned that its my choice whether I can tick mark the options like &quot;Send anonymous stats to our portal&quot;,&quot;Share your installation details to public&quot;...etc. So, I started customized installation and selected only the services I need leaving the other services uninstalled.</p>
<h3 id="ok-what-if-i-have-already-installed-with-default-settings-">OK, What if I have already installed with default settings:</h3>
<p>In most of the windows systems, if you uninstall any software the configuration data is not deleted. Usually every software will be installed in &quot;Program Files&quot; directory and the relevant settings/data will be saved at user&#39;s profile &quot;Appdata&quot; directory. So you can uninstall and reinstall the software. The other option is, most of the software allow us to customize even after installing them in their options menu. Better to choose first option and reinstall software.</p>
<p>In software(Ethical one), The only way to get our data is directly asking us in any of the above manner.. And if so, compliance like GDPR also cannot help us at that moment.</p>
