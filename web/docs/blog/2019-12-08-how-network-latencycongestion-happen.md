---
layout: post
title: 'How network latency/congestion happen?'
---
<h3 id="nat-principle-">NAT Principle:</h3>
<p>Generally at our home we have routers which can support upto 20 users. How can it hold only for 20 users? Okay its not about 20 users, it is about 20 concurrent connections. Generally any router does the NAT mechanism to transmit the data from intranet to internet. Look at the below pic:</p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1575781197673/Ggdh-cv_Q.png" alt="nat.png">
Here in the above case all the LAN computers are accessing Internet resources like Google. Now in the gateway level a NAT will take all the internal IP&#39;s and convert it to a single external IP. Always we should remember that we give only destination port to access the resources and source port will be taken randomly by the application. Inside the LAN source ports are 1025 from different computers and gateway changed them to 8888, 8889, 8890 etc for single IP.</p>
<h3 id="buffer-">Buffer:</h3>
<p>OK now for example if we connect 10000 pc&#39;s inside LAN then our gateway assigns 10000 random ports and communicates with its gateway.  By this we can conclude that any gateway cannot hold more than 63750 concurrent connections. Whenever the LAN computer connects with the Internet our gateway has to create some buffer so that it can forward packets from ISP network to LAN. Each router is designed to hold some amount of buffer in the form of connections. If the number of connections increase then the buffer at gateway also increases and finally if the gateway reaches the maximum buffer it can&#39;t take any new connections. We can eliminate this by creating micro networks inside our main network. Placing many number of hosts in a single LAN network can cause serious latency issues because of memory/processor overload.</p>
