---
layout: post
title: 'What should be the domain name for our intranet!'
---
<h3 id="why-do-we-need-a-domain-in-the-intranet-">Why do we need a domain in the intranet?</h3>
<p>While installing few softwares they need fully qualified domain name of a server to get discovered in the network. Eg softwares are Freeipa, cpanel etc.. If the software is hosted in public server, then we can buy a domain name and map that domain name to public server.  But, we cannot assign a public domain to our private network/servers as it is illegal.</p>
<h3 id="-local-domain-">.local Domain:</h3>
<p>Do we have any private domains like private ip address so what we can use it in our intranet? What if we need a domain name for installing software inside our NAT network?  Yes, we can make use of &quot;.local&quot; domain. This is basically an internal TLD(Top level domain) similar to private ip address.</p>
<h3 id="use-case-">Use case:</h3>
<p>We can use this domain to assign fqdn to any number of hosts. This domain doesn&#39;t need any registration. To such servers which need domain name and fqdn we should assign hostname as follows:</p>
<ul>
<li>Domain name: &quot;example.local&quot;</li>
<li>Server FQDN: &quot;server.example.local&quot;</li>
<li>Clients FQDN: &quot;client.example.local&quot;</li>
</ul>
<p>To make domain name autodiscovery easy, just follow these steps:</p>
<ul>
<li>Keep gateway name as &quot;gateway.example.local&quot;</li>
<li>Add static entries of domain name and static hosts in the default dns server.</li>
<li>Enable auto update dns records from DHCP registered clients in gateway</li>
<li>Configure all hosts to query default DNS server</li>
</ul>
<p>Done, this way we can make use of &quot;.local&quot; domain eliminating hostname conflicts when you encounter mandatory domain name issue.</p>
<p><strong>Note:</strong> &quot;example&quot; domain name is given just for an example, anyone can assign any dns name to their network in &quot;.local&quot; TLD.</p>
