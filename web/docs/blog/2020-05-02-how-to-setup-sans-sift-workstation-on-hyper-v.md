---
layout: post
title: 'How to setup SANS sift workstation on Hyper-V?'
---
<h3 id="sans-sift-">Sans SIFT:</h3>
<p><strong>Sans SIFT</strong> is an Opensource SANS Investigative Forensics Toolkit which is used to perform disk Forensic analysis based on Linux. It has the popular tools like autopsy, plaso, dd, wireshark etc. </p>
<p>This article drives through the installation of Sift on Hyper-V. Go to Google and hit sans sift as follows.
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588443383615/IMDtkvbJe.png" alt="Screenshot from 2018-11-13 21-38-48.png"></p>
<p>After landing into  <a target='_blank' rel='noopener noreferrer'  href="https://digital-forensics.sans.org/community/downloads">sans sift page</a> , just click on Download virtual appliance(.ova file). We should remember that we need SANS account to download SANS resources.The second option is install all the SANS tools into ubuntu machine.</p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588443535868/eC_e4eqcM.png" alt="Screenshot from 2018-11-13 21-39-30.png">
Select the .ova file and download.</p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588443569083/LFpQpZEr9.png" alt="Screenshot from 2018-11-13 21-40-30.png">
An <strong>ova </strong> file is an Open Virtualization Format file, it is an open standard compression for virtual images. After downloading, navigate to the .ova file, rename it with &quot;.zip&quot; and extract the file.</p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588443588290/X1ZYuju7s.png" alt="Screenshot from 2018-11-13 21-56-51.png">
Inside the extracted folder we have 3 files. For Virtualbox we can directly import the .ova file but for the Hyper-V we need .vhd file as the virtual hard drive.
To convert the vmdk to vhd we need to run the following command from the cmd/terminal.
<code>vboxmanage clonehd --format vhd &lt;source file&gt;.vmdk &lt;destination file&gt;.vhd</code></p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588443671345/1TLeg7p09.png" alt="Screenshot from 2018-11-13 22-05-20.png">
Here we are using Virtualbox cli tool called &quot;VboxManage&quot; and creating a vhd file with given vmdk file.</p>
<p><img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588443708203/oImeIMVV1.png" alt="Screenshot from 2018-11-13 22-06-14.png">
Now that we can directly import vhd file into Hyper-V server.</p>
