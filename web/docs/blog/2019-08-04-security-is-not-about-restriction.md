---
layout: post
title: 'Security is not about restriction!'
---
<p>Now a days if someone ask us to secure an application, we would recommend to block something. Yeah, blocking solves the problem for that instance. But up-to what extent this solution can withstand? What if we are in a situation where we must allow that port/service open?</p>
<h2 id="possibilities-">Possibilities:</h2>
<p>Lets consider the google&#39;s case: How google became popular? Because it will give whatever you ask for, query can be any technical question or even bull shit. Similarly as a security engineer, we have to allow whatever our employee/application need to perform work. Security wont drive the business, rather it will add some value to the business. We should know this thin line to create an application work in its own way and in secure way. Instead of restricting something i.e websites, ports etc.., we can allow them and monitor that specific traffic. Creating honeypots can help us trapping the attacker. Monitoring even helps us to get to know the insights of employees. Now its the right time to shifting our rules from restricted to partially allow with monitoring.</p>
