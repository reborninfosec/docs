---
layout: post
title: 'Infosec job: Neither a product based nor a project project based!'
---
<p>I often hear this question from my friends/relatives about the kind of company I work &quot;Is your company is <strong>product based </strong>or<strong> project based?&quot;</strong>. Each one has its own myths to believe. As a infosec engineer, my answer for this question is Neither product based nor project based.</p>
<p>You may argue with me why is it so, but I can only say I work for better security no matter what whether our company builds product or do some project for others. I work to secure assets. Assets would be product, project, process, people, internal Infra, external Infra etc.. The biggest challenge for Infosec job is there will be no evaluation for the role. And sometimes it is hard to answer questions about what are we doing. If it is a software development one can simply say I am developing this feature but for an infosec personnel it&#39;s just monitoring/thinking/restricting/creating which doesn&#39;t produce any output but definitely brings outcome. It is also recommended for our organisations to be compliant to ISO 27001. This can be used in both ways</p>
<ol>
<li>To prove infosec work </li>
<li>Add value to the product/project compared to our competition.</li>
</ol>
