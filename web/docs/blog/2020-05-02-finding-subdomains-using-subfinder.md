---
layout: post
title: 'Finding subdomains using subfinder'
---
<p><strong>Subdomain: </strong> A subdomain concept is used to host multiple domains under single parent domain.</p>
<p>Eg:
Parent Domain = &quot;google.com&quot;
Subdomains = &quot;mail.google.com&quot;, &quot;drive.google.com&quot;, &quot;accounts.google.com&quot;.. etc.</p>
<p>Most of the organizations host multiple applications in multiple subdomains as mentioned in the example &quot;google&quot;. We can use subdomain finders during red team reconnaissance process. We use the below tool to find subdomains from various opensource databases(OSINT) and organize them into a single list. Later we can initiate penetration testing iterating each domain.</p>
<p>Tool:  <a target='_blank' rel='noopener noreferrer'  href="https://github.com/projectdiscovery/subfinder">Subfinder</a> </p>
<h2 id="installation-">Installation:</h2>
<p><strong>Prerequisites:</strong> This tool is based on &quot;go&quot; so we need go installed in the machine. To install go follow these instructions.  https://golang.org/doc/install#install </p>
<p>After installling go, clone the subfinder repository and install</p>
<pre><code>$ git <span class="hljs-built_in">clone</span> https://github.com/subfinder/subfinder.git
$ <span class="hljs-built_in">cd</span> subfinder
$ ./build.sh
</code></pre><p>The command <code>build.sh</code> builds the binary for subfinder and stores in build folder.</p>
<p>Goto build folder and unzip the zip file using &quot;unzip&quot; command.</p>
<pre><code>$ cd build
$ unzip subfinder_linux_amd64_1.<span class="hljs-number">0</span>.zip <span class="hljs-comment">#version may be different</span>
</code></pre><p>Usage:</p>
<pre><code>$ ./subfinder -h <span class="hljs-comment">#shows the help message and all the options</span>
$ ./subfinder -d example.com -v -o filename.txt <span class="hljs-comment">#basic command usage</span>
</code></pre><p>The above command finds all the subdomains and stores all the output data to filename.txt file. We can change all the options to our custom names.</p>
<p>Sample screenshot:
<img src="https://cdn.hashnode.com/res/hashnode/image/upload/v1588445744531/xzGeL3Uq4.png" alt="Screenshot from 2018-11-29 19-43-30.png">
If we plan for automation this tool result can be handy, with the output we can pass all the subdomains to the other tool as input. Subfinder supports multiple api&#39;s like shodan, virustotal, Cencys .. etc for better results.</p>
<p>Please note that running subfinder comes under passive online attack.</p>
