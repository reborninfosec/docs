---
layout: post
title: 'why organizations should use leased line?'
---
<p>Technically speaking there are two types of Internet subscription: <strong>Leased line</strong> and <strong>Broadband</strong>. In broadband internet the main connection is divided into multiple switched connections at ISP level and the access for users are given by radius server. In leased line the internet line is directly pulled from telephonic multiplexers.</p>
<p>Coming to the point, one important reason why organizations need leased line is to maintain security. Mostly broadband connections are layed to area wise and each area will have a fibre connection. Using fibre optic media converter, the connection converts to ethernet and this ethernet is connect to a network switch. This switch can be placed at any one home who offered some space for it. Now this is the point which need some emphasis. If any organisation is taking broadband connection then they may definitely have the following risks:</p>
<ul>
<li><strong>Intercept:</strong> First and foremost is anyone in that area where switch is placed can easily intercept all the network traffic. Thanks to network taps and Lan turtle to implement this lot more easier.</li>
<li><strong>Power loss:</strong> Network will be disconnected if there is any power loss in that home.</li>
<li><strong>Public IP:</strong> They will offer public IP but may not be static.</li>
</ul>
<p>Apart from the above there may be other issues that organisation&#39;s could face. The only advantage is they can get good speed compared to leased line.</p>
<p>While if they use leased line, the connection would be fibre optic one and there are still ways to intercept fibre optic but very costly. Second thing is the SLA, All the ISP offer leased line with better SLA with very less downtime and proper support is provided.</p>
<p>Leased line is bit costly compared to broadband connections but it gives reliable throughput. </p>
