---
layout: post
title: 'How total protection is also dangerous'
---
<h3 id="security-methodologies-">Security methodologies:</h3>
<p>Day to day we are implementing new security methodologies like encryption, otp, biometric etc.. to prevent unauthorized access of our data. Because we don&#39;t trust anyone except us, we never keep these methodologies shareable to anyone. Ok, Now the problem is what if we lost the private key to decrypt the data? What if we lost our mobile which receives an otp? Do we have any backup strategy for such cases?</p>
<h3 id="two-sides-of-the-same-coin-">Two sides of the same coin.</h3>
<p>Consider we have built an igloo. That igloo has only only one entry point. Now the scenario is something like, a snake entered inside the igloo. There is only entry/exit point and now the person inside igloo can&#39;t escape from snake.  We can correlate the igloo example with organization/personal digital assets. Here organizational infra is the igloo, snake is any cyber attack. Remember, we will be so secure until someone enters into our network, but once someone got to know our secret recipe(any vulnerability) then that is our end.</p>
<h3 id="backup-strategy-">Backup strategy:</h3>
<p>A backup strategy is nothing but proactive move for the reactive situation. Always we should have a second way to get out of situation. It can be anything such as backing the otp passwords, private key and creating a redundant plan to recover from disaster as soon as possible. Henceforth, we should develop a resilient methodologies where we could recover from disaster with minimum downtime.</p>
