import docker
import os
client = docker.from_env()
# client.images.pull('reborninfosec/secdev')
client.images.pull('reborninfosec'+'/'+os.environ['CI_PROJECT_NAME'])
lbl = {'label':'app=web'}
for container in client.containers.list(all):
    if container.name == 'web':
        container.stop()
        container.remove()
client.containers.run('reborninfosec'+'/'+os.environ['CI_PROJECT_NAME'], labels=lbl,hostname='web',name='web',detach=True,network='secure-nw')